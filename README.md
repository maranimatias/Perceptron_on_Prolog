# Perceptron On Prolog
Implementation of a simple perceptron in Prolog.

## Perceptron

![Perceptron Image](https://raw.githubusercontent.com/MaraniMatias/Perceptron_on_Prolog/image/percepto.png)

## Problem

Sort numbers in even, odd, or greater than 5

![7 Segment Display Image](https://raw.githubusercontent.com/MaraniMatias/Perceptron_on_Prolog/image/7_segment_display.png)


## Run

```Prolog
?- epoch(20).
```
